#include "fft_header.cuh"

__global__ void zero_pad_input(cufftReal * d_out, cufftReal * d_in,int M,int N,int I,int J){
	// - I,J after padding
	// - M,N before padding
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < M && j < N)
				d_out[i*J + j] = d_in[i*N + j];
	else if (i < I && j < J)
				d_out[i*J + j]  = 0.0f;
	}

__global__ void expand_output(cufftComplex * d_out, cufftComplex * d_in,int I,int J){
//    	 	int j = threadIdx.x;
//    	    int i = threadIdx.y;

	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	
	if ( j < J/2 + 1 && i < I)
		d_out[i*J + j] = d_in[i*(J/2+1) + j];
	else if ( i == 0 && j < J)
		{ 
		d_out[i*J + j].x = d_in[i*(J/2+1) + (J-j)].x;
		d_out[i*J + j].y = -d_in[i*(J/2+1) + (J-j)].y;
		}
	else if (i < I && j < J)
		{
		d_out[i*J + j].x = d_in[(I-i)*(J/2+1) + (J-j)].x;
		d_out[i*J + j].y = -d_in[(I-i)*(J/2+1) + (J-j)].y;
		}

	}

__global__ void zero_pad_input_pitched(cufftReal * d_out, cufftReal * d_in,int M,int N,int I,int J,size_t ipitch,size_t opitch){
	// - I,J after padding
	// - M,N before padding
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x; // row index
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y; // col index

	if (i < M && j < N)
				d_out[i*opitch/sizeof(cufftReal) + j] = d_in[i*ipitch/sizeof(cufftReal) + j];
	else if (i < I && j < J)
				d_out[i*opitch/sizeof(cufftReal) + j]  = 0.0f;
	}

__global__ void expand_output_pitched(cufftComplex * d_out, cufftComplex * d_in,int I,int J,size_t pitch){
	unsigned int i = blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;

	if ( j < J/2 + 1 && i < I)
		d_out[i*J + j]  =   d_in[i*pitch/sizeof(cufftComplex) + j];
	else if (j < J && i == 0)
		{ 
		d_out[i*J + j].x =  d_in[i*pitch/sizeof(cufftComplex) + (J-j)].x;
		d_out[i*J + j].y = -d_in[i*pitch/sizeof(cufftComplex) + (J-j)].y;
		}
	else if ( i < I && j < J)
		{
		d_out[i*J + j].x =  d_in[(I-i)*pitch/sizeof(cufftComplex) + (J-j)].x;
		d_out[i*J + j].y = -d_in[(I-i)*pitch/sizeof(cufftComplex) + (J-j)].y;
		}

	}


void forward_tf()
	{

// 	 	const size_t M = 4;
// 	 	const size_t N = 6;
// 		const size_t I = 10;
// 		const size_t J = 8;
// 	 	// Input array (static) - host side 
// 	 	float h_in_data_static[M][N] ={ 
// 	 		{0.7943 ,   0.6020 ,   0.7482  ,  0.9133  ,  0.9961 , 0.9261 },
// 	 		{0.3112 ,   0.2630 ,   0.4505  ,  0.1524  ,  0.0782 , 0.1782 },
// 	 		{0.5285 ,   0.6541 ,   0.0838  ,  0.8258  ,  0.4427,  0.3842 },
// 	 		{0.1656 ,   0.6892 ,   0.2290  ,  0.5383  ,  0.1067,  0.1712 }
// 	 		};

  	const size_t M = 4;
  	const size_t N = 7;
  	const size_t I = 100;
  	const size_t J = 100;
  	float h_in_data_static[M][N] ={ 
  		{0.7943 ,   0.6020 ,   0.7482  ,  0.9133  ,  0.9961 , 0.9261, 0.3134},
  		{0.3112 ,   0.2630 ,   0.4505  ,  0.1524  ,  0.0782 ,  0.1782, 0.4213},
  		{0.5285 ,   0.6541 ,   0.0838  ,  0.8258  ,  0.4427,  0.3842,  0.211},
  		{0.1656 ,   0.6892 ,   0.2290  ,  0.5383  ,  0.1067,  0.1712,  0.3313}
  		};



	// --------------------------------
	// Input array (dynamic) - host side 
	float *h_in_data_dynamic = new float[M*N];  

	// Set the values
	size_t h_ipitch;
	for (int r = 0; r < M; ++r)  // this can be also done on GPU
		{  	 
		for (int c = 0; c < N; ++c)
			{	h_in_data_dynamic[N*r + c] = h_in_data_static[r][c];	}
		}
	// --------------------------------

	// Output array - host side
	float2 *h_out_data = new float2[I*J]; 
	cufftComplex * d_out_data;
	cudaMalloc((void**)&d_out_data, sizeof(cufftComplex)*I*J);
	 
	int mode = 1;
	switch (mode)
		{
	case 0 :
		// ***************************
		// FFT - with pitched arrays	
		cout << "pitched fft" << endl;
		fft_R2C_pitched(d_out_data,h_in_data_dynamic,M,N,I,J);
		// ***************************
		break;
	case 1 : 
		// ***************************
		// FFT - normal arrays 
		cout << "normal fft1" << endl;
		fft_R2C(d_out_data,h_in_data_dynamic,M,N,I,J);
		// ***************************
		break;
		}

	  

	cudaError  cudaStat2 = 	cudaMemcpy(h_out_data,d_out_data,  sizeof(cufftComplex)*I*J , cudaMemcpyDeviceToHost); 
	cout << cudaGetErrorString(cudaStat2) << endl;
	// Print the results
	for (int i = 0; i < I; i++)	
		{
		for (int j =0 ; j< J ; j++)		
			printf(" %.4f ",h_out_data[i*J + j].x   );
		printf("\n");	 
		}
	 
	}

void fft_R2C_pitched(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J){

	// Output array - host side
	float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	// Input and Output array - device side	
	cufftHandle plan;
	cufftReal *d_in_data;      
	cufftReal *d_in_data_padded;       // Input array - device side	
	
	cufftComplex * d_out_data_temp;
	int n[NRANK] = {I, J};

	
	 
	//  Copy input array from Host to Device
	size_t ipitch1,ipitch2;
	cudaError  cudaStat1 = 	cudaMallocPitch((void**)&d_in_data_padded,&ipitch1,J*sizeof(cufftReal),I);		
	cudaError  cudaStat11 = cudaMallocPitch((void**)&d_in_data,&ipitch2,N*sizeof(cufftReal),M);	
	cudaError  cudaStat2 = 	cudaMemcpy2D(d_in_data,ipitch2,h_in_data,N*sizeof(float),N*sizeof(float),M,cudaMemcpyHostToDevice);


	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J + blockDim.y - 1) / blockDim.y);
	zero_pad_input_pitched<<<gridDim,blockDim>>>(d_in_data_padded,d_in_data,M,N,I,J,ipitch2,ipitch1);

	//  Allocate memory for output array - device side
	size_t opitch;
	cudaError  cudaStat3 = 	cudaMallocPitch((void**)&d_out_data_temp,&opitch,(J/2+1)*sizeof(cufftComplex),I);	
	
	//  Performe the fft
	int rank = 2; // 2D fft     
	int istride = 1, ostride = 1; // Stride lengths
	int idist = 1, odist = 1;     // Distance between batches
	int inembed[] = {I, ipitch1/sizeof(cufftReal)}; // Input size with pitch
	int onembed[] = {I, opitch/sizeof(cufftComplex)}; // Output size with pitch
	int batch = 1;
	cufftPlanMany(&plan, rank, n, inembed, istride, idist, onembed, ostride, odist, CUFFT_R2C, batch);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecR2C(plan, d_in_data_padded, d_out_data_temp);
	cudaThreadSynchronize();

	// Copy d_in_data back from device to host
	// 	cudaError  cudaStat5 = cudaMemcpy2D(h_out_data_temp,(J/2+1)*sizeof(float2), d_out_data_temp, opitch, (J/2+1)*sizeof(cufftComplex), I, cudaMemcpyDeviceToHost); 
	// 	//cout << cudaGetErrorString(cudaStat5) << endl;
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		{
	// 		for (int j = 0; j < J ; j++)
	// 			{
	// 			if ( j < J/2 + 1)
	// 				h_out_data[i*J + j] = h_out_data_temp[i*(J/2+1) + j];
	// 			else if ( i == 0)
	// 				{ 
	// 				h_out_data[i*J + j].x = h_out_data_temp[i*(J/2+1) + (J-j)].x;
	// 				h_out_data[i*J + j].y = -h_out_data_temp[i*(J/2+1) + (J-j)].y;
	// 				}
	// 			else 
	// 				{
	// 				h_out_data[i*J + j].x = h_out_data_temp[(I-i)*(J/2+1) + (J-j)].x;
	// 				h_out_data[i*J + j].y = -h_out_data_temp[(I-i)*(J/2+1) + (J-j)].y;
	// 				}
	// 			}
	// 		}

	expand_output_pitched<<<gridDim,blockDim>>>(d_out_data,d_out_data_temp,I,J,opitch);  // d_in_data - halved matrix - input to ifft2 - C2R
	


	cufftDestroy(plan);	
	cudaFree(d_in_data);
	//cudaFree(d_out_data);	
	cudaFree(d_out_data_temp);


	}

void fft_R2C(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J){

	// Output array - host side
	float2 *h_out_data_temp = new float2[I*(J/2+1)] ; 

	cufftHandle plan;
	cufftReal *d_in_data;       // Input array - device side
	cufftReal *d_in_data_padded;       // Input array - device side	
	      // Output array - device side - full array 
	cufftComplex * d_out_data_temp;  // Output array - device side - half array (output from the fft2)
	 
	 
	cudaMalloc((void**)&d_in_data_padded, sizeof(cufftReal)*I*J);
	cudaMalloc((void**)&d_in_data, sizeof(cufftReal)*M*N);
	cudaMemcpy(d_in_data, h_in_data, sizeof(cufftReal)*M*N, cudaMemcpyHostToDevice);    	 

	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J + blockDim.y - 1) / blockDim.y);
	zero_pad_input<<<gridDim,blockDim>>>(d_in_data_padded,d_in_data,M,N,I,J);

	 
	cudaMalloc((void**)&d_out_data_temp, sizeof(cufftComplex)*I*(J/2 + 1));
	


	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecR2C(plan, d_in_data_padded, d_out_data_temp);
	cudaThreadSynchronize();

	// Exapnding the output in to the full matrix - on CPU
	// 	cudaError  cudaStat2 = 	cudaMemcpy(h_out_data_temp,d_out_data_temp,  sizeof(cufftComplex)*I*(J/2+1), cudaMemcpyDeviceToHost);
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		{
	// 		for (int j = 0; j < J ; j++){
	// 			if ( j < J/2 + 1)
	// 				h_out_data[i*J + j] = h_out_data_temp[i*(J/2+1) + j];
	// 			else if ( i == 0)
	// 				{ 
	// 				h_out_data[i*J + j].x = h_out_data_temp[i*(J/2+1) + (J-j)].x;
	// 				h_out_data[i*J + j].y = -h_out_data_temp[i*(J/2+1) + (J-j)].y;
	// 				}
	// 			else 
	// 				{
	// 				h_out_data[i*J + j].x = h_out_data_temp[(I-i)*(J/2+1) + (J-j)].x;
	// 				h_out_data[i*J + j].y = -h_out_data_temp[(I-i)*(J/2+1) + (J-j)].y;
	// 				}
	// 			}
	// 		}

	// Exapnding the output in to the full matrix - on GPU

	// Halve the input array
// 	dim3 blockDim(16, 16);
// 	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J + blockDim.y - 1) / blockDim.y);
	 
	expand_output<<<gridDim,blockDim>>>(d_out_data,d_out_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	
 
	cufftDestroy(plan);
	//cudaFree(d_out_data);
	cudaFree(d_out_data_temp);
	cudaFree(d_in_data); 
	cudaFree(d_in_data_padded);	



	}