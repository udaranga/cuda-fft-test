#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cufft.h>
#include <stdio.h> 
#include <iomanip> 
#include <iostream>
#include <vector>
#include <ctime>

using namespace std;

#define NRANK 2
#define BATCH 10





void fft_R2C_pitched(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J);
void fft_R2C(cufftComplex * d_out_data,float *h_in_data,int M,int N,int I,int J);
void fft_C2R_pitched(float *h_out_data,float2 *h_in_data,int I,int J);
void fft_C2R(float *h_out_data,float2 *h_in_data,int I,int J);
void forward_tf();
void backward_tf();