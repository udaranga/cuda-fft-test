#include "fft_header.cuh"

// For Normal FFT
__global__ void halve_input(cufftComplex * d_out, cufftComplex * d_in,int I,int J){
// 	int i = threadIdx.x;
// 	int j = threadIdx.y;
	unsigned int i= blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	if ( i < I && j < J/2 + 1)
		d_out[i*(J/2+1) + j] = d_in[i*J + j];		
	}

__global__ void normalize_ifft(cufftReal * d_out, cufftReal * d_in,int I,int J){
	int i = threadIdx.x;
	if ( i < I*J) 
		d_out[i] = d_in[i]/(I*J);
	}


// For pitched FFT
__global__ void halve_input_pitched(cufftComplex * d_out, cufftComplex * d_in,int I,int J,size_t ipitch,size_t opitch){
// 	int i = threadIdx.x;
// 	int j = threadIdx.y;
	// Calculate normalized texture coordinates
	unsigned int i= blockIdx.x * blockDim.x + threadIdx.x;
	unsigned int j = blockIdx.y * blockDim.y + threadIdx.y;
	if ( i < I && j < J/2 + 1)
		d_out[i*opitch/sizeof(cufftComplex) + j] = d_in[i*ipitch/sizeof(cufftComplex) + j];		
	}

__global__ void normalize_ifft_pitched(cufftReal * d_out, cufftReal * d_in,int I,int J,size_t pitch){
	int i = threadIdx.x;
	int j = threadIdx.y;
	if ( i < I && j < J) 
		d_out[i*pitch/sizeof(cufftReal) + j] = d_in[i*pitch/sizeof(cufftReal) + j]/(I*J);
	}

void backward_tf()
	{

// 	const size_t NX = 4;
// 	const size_t NY = 6;
// 	// Input array (static) - host side 
// 	float h_in_data_static_real[NX][NY] ={
// 		{ 11.2326 ,  -0.2638 ,  0.7278 ,  -1.3630 ,   0.7278  , -0.2638 },
// 		{ 2.0609  , -0.0270  ,  0.0912 ,   0.9063 ,  -1.0922  , -0.3444 },
// 		{ 4.5656  , -0.2732  ,  0.5588 ,  -0.0608 ,   0.5588  , -0.2732 },
// 		{ 2.0609  , -0.3444  , -1.0922 ,   0.9063 ,   0.0912  , -0.0270 }		
// 		};
// 
// 	float h_in_data_static_imag[NX][NY] ={ 
// 		{0       , -0.3779 , -0.5723  ,       0   , 0.5723   ,   0.3779     },
// 		{0.4665  ,  0.1929 ,  0.7377  ,  -1.1437  , -0.4833  , -0.6437      },
// 		{0       ,  1.5228 , -0.3849  ,        0  ,   0.3849 ,  -1.5228     },
// 		{-0.4665 ,  0.6437 ,  0.4833  ,   1.1437  , -0.7377  ,  -0.1929}
// 		};

	const size_t NX = 4;
	const size_t NY = 7;
	// Input array (static) - host side 
	float h_in_data_static_real[NX][NY] ={
		{12.5096 ,  -0.3851 ,   0.6942 ,  -0.2653  , -0.2653 ,   0.6942  , -0.3851 },
		{ 2.1633 ,  -0.1992 ,  -0.1157 ,   1.5701  ,  0.0899 ,  -0.7495  , -0.8982 },
		{ 4.3374 ,  -1.4289 ,   1.2615 ,   0.9597  ,  0.9597 ,   1.2615  , -1.4289 },
		{ 2.1633 ,  -0.8982 ,  -0.7495 ,   0.0899  ,  1.5701 ,  -0.1157  , -0.1992 }	
		};

	float h_in_data_static_imag[NX][NY] ={ 
		{      0  , -0.9334 ,  -0.3420 ,  -1.3058 ,   1.3058 ,   0.3420 ,   0.9334 },
		{ 0.3765  , -0.0550 ,   0.0834 ,  -0.0662 ,  -1.3002 ,   0.4043 ,  -0.4620 },
		{      0  ,  0.4613 ,  -1.0302 ,  -0.6626 ,   0.6626 ,   1.0302 ,  -0.4613 },
		{-0.3765  ,  0.4620 ,  -0.4043 ,   1.3002 ,   0.0662 ,  -0.0834 ,   0.0550 }
		};

	float2 h_in_data_static[NX][NY];
	for (int i = 0; i < NX ; i++)
		for (int j = 0; j < NY ; j++)
			{
			h_in_data_static[i][j].x = h_in_data_static_real[i][j];
			h_in_data_static[i][j].y = h_in_data_static_imag[i][j];
			}


		// --------------------------------
		// Input array (dynamic) - host side 
		float2 *h_in_data_dynamic = new float2[NX*NY];  

		// Set the values
		size_t h_ipitch;
		for (int r = 0; r < NX; ++r)  // this can be also done on GPU
			{  	 
			for (int c = 0; c < NY; ++c)
				{	h_in_data_dynamic[NY*r + c] = h_in_data_static[r][c];	}
			}
		// --------------------------------

		// Output array - host side
		float *h_out_data = new float[NX*NY] ; 
		int N = 1;
		double elapsed_secs = 0;
		int mode = 0;
		switch (mode)
			{
		case 0 :
			// ***************************
			// FFT - with pitched arrays
			cout << "pitched ifft2" << endl;
			fft_C2R_pitched(h_out_data,h_in_data_dynamic,NX,NY);
			// ***************************
			break;
		case 1 : 
			// ***************************
			// FFT - normal arrays 
			cout << "normal ifft2" << endl;
			fft_C2R(h_out_data,h_in_data_dynamic,NX,NY);
			// ***************************
			break;
			}
		

		// Print the results
		for (int i = 0; i < NX; i++)	
			{
			for (int j =0 ; j< NY ; j++)		
				printf(" %f  ",h_out_data[i*NY + j]);
			printf("\n");	 
			}
	}

void fft_C2R_pitched(float *h_out_data,float2 *h_in_data,int I,int J){

	// ***************************
	// Output array - host side
	// 	float2 *h_in_data_temp = new float2[I*(J/2+1)] ; 
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		for (int j = 0; j < J ; j++)
	// 			if ( j < J/2 + 1)
	// 				h_in_data_temp[i*(J/2+1) + j] = h_in_data[i*J + j];
	// ***************************
 

	// Input and Output array - device side	
	cufftHandle plan;
	cufftComplex *d_in_data;       // Input array half - device side
	cufftComplex *d_in_data_temp;  // Input array full- device side
	cufftReal * d_out_data;
	int n[NRANK] = {I, J};

	size_t ipitch,ipitch_temp;
	// ***************************
	//  Copy input array from Host to Device - already halved matrix	
	// 	cudaError  cudaStat1 = 	cudaMallocPitch((void**)&d_in_data,&ipitch,(J/2+1)*sizeof(cufftComplex),I);		 
	// 	cudaError  cudaStat2 = 	cudaMemcpy2D(d_in_data,ipitch,h_in_data_temp,(J/2+1)*sizeof(float2),(J/2+1)*sizeof(float2),I,cudaMemcpyHostToDevice);   
	// ***************************

	// ***************************
	//  Copy input array from Host to Device and then get the first half of the matrix
	//size_t ipitch;
	cudaError  cudaStat3 = 	cudaMallocPitch((void**)&d_in_data,&ipitch,(J/2+1)*sizeof(cufftComplex),I);		 
 	cudaError  cudaStat4 = 	cudaMallocPitch((void**)&d_in_data_temp,&ipitch_temp,J*sizeof(cufftComplex),I);		 
 	cudaError  cudaStat5 = 	cudaMemcpy2D(d_in_data_temp,ipitch_temp,h_in_data,J*sizeof(float2),J*sizeof(float2),I,cudaMemcpyHostToDevice);   
 	
 	 
  	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J/2 + 1 + blockDim.y - 1) / blockDim.y);
	halve_input_pitched<<<gridDim,blockDim>>>(d_in_data,d_in_data_temp,I,J,ipitch_temp,ipitch);  // d_in_data - halved matrix - input to ifft2 - C2R
	// ***************************


	//  Allocate memory for output array - device side
	size_t opitch;
	cudaError  cudaStat6 = 	cudaMallocPitch((void**)&d_out_data,&opitch,J*sizeof(cufftReal),I);	

	//  Performe the fft
	int rank = 2; // 2D fft     
	int istride = 1, ostride = 1; // Stride lengths
	int idist = 1, odist = 1;     // Distance between batches
	int inembed[] = {I, ipitch/sizeof(cufftComplex)}; // Input size with pitch
	int onembed[] = {I, opitch/sizeof(cufftReal)}; // Output size with pitch
	int batch = 1;
	cufftPlanMany(&plan, rank, n, inembed, istride, idist, onembed, ostride, odist, CUFFT_C2R, batch);
	//cufftPlan2d(&plan, I, J , CUFFT_R2C);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecC2R(plan, d_in_data, d_out_data);
	cudaThreadSynchronize();

	// ***************************
	// Noramlize the output - on GPU
	dim3 blockDim2(16, 16);
	dim3 gridDim2((I + blockDim.x - 1) / blockDim.x , (J + blockDim.y - 1) / blockDim.y);
	normalize_ifft_pitched<<<gridDim2,blockDim2>>>(d_out_data,d_out_data,I,J,opitch);
	// Copy d_in_data back from device to host
	cudaError  cudaStat11 = cudaMemcpy2D(h_out_data,J*sizeof(float), d_out_data, opitch, J*sizeof(cufftReal), I, cudaMemcpyDeviceToHost); 
	// ***************************

	// ***************************
	// Normalize the output - on CPU
	// 	for (int i = 0;i < I*J;i++)
	// 		h_out_data[i] = h_out_data[i]/(I*J);
	// ***************************

	cufftDestroy(plan);	
	cudaFree(d_out_data);
	cudaFree(d_in_data);

	}

void fft_C2R(float *h_out_data,float2 *h_in_data,int I,int J){

	// ***********************
	// Make the half matrix on CPU 
	// Output array - host side
	// 	float2 *h_in_data_temp = new float2[I*(J/2+1)] ; 
	// 
	// 	for (int i = 0; i < I ; i++)
	// 		for (int j = 0; j < J ; j++)
	// 			if ( j < J/2 + 1)
	// 				h_in_data_temp[i*(J/2+1) + j] = h_in_data[i*J + j];
	// ***********************

	cufftHandle plan;
	cufftComplex *d_in_data;       // Input array half - device side
	cufftComplex *d_in_data_temp;  // Input array full- device side
	cufftReal * d_out_data;  // Output array - device side
	int n[NRANK] = {I, J};

	cudaMalloc((void**)&d_in_data, sizeof(cufftComplex)*I*(J/2 + 1));
	//cudaMemcpy(d_in_data, h_in_data_temp, sizeof(cufftComplex)*I*(J/2 + 1), cudaMemcpyHostToDevice);  // Uncomment if it is already haved in CPU
	
	// ******************
	// Halve the input in gpu
 	cudaMalloc((void**)&d_in_data_temp, sizeof(cufftComplex)*I*J);                    
 	cudaMemcpy(d_in_data_temp, h_in_data, sizeof(float2)*I*J, cudaMemcpyHostToDevice);

	// Halve the input array
	dim3 blockDim(16, 16);
	dim3 gridDim((I + blockDim.x - 1) / blockDim.x , (J/2 + 1 + blockDim.y - 1) / blockDim.y);
	halve_input<<<gridDim,blockDim>>>(d_in_data,d_in_data_temp,I,J);  // d_in_data - halved matrix - input to ifft2 - C2R
	// ******************

	cudaMalloc((void**)&d_out_data, sizeof(cufftReal)*I*J);

	//Performe the fft
	cufftPlan2d(&plan, I, J , CUFFT_C2R);
	cufftSetCompatibilityMode(plan, CUFFT_COMPATIBILITY_NATIVE);
	cufftExecC2R(plan, d_in_data, d_out_data);
	cudaThreadSynchronize();

	// Noramlize the output - on GPU
	dim3 blockDim2(32);
	dim3 gridDim2((I*J + blockDim.x - 1) / blockDim.x);
	normalize_ifft<<<gridDim2,blockDim2>>>(d_out_data,d_out_data,I,J);

	cudaError  cudaStat2 = 	cudaMemcpy(h_out_data,d_out_data,  sizeof(cufftReal)*I*J, cudaMemcpyDeviceToHost);

	// Normalize the output - on CPU
// 	for (int i = 0;i < I*J;i++)
// 		h_out_data[i] = h_out_data[i]/(I*J);

	cufftDestroy(plan);
	cudaFree(d_in_data_temp);
	cudaFree(d_in_data);
	cudaFree(d_out_data);


	}